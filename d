[33mcommit fdef0046e3f124341f0e6fb04faf73b4d89b8bd5[m[33m ([m[1;36mHEAD -> [m[1;32mmain[m[33m, [m[1;31morigin/main[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Merge: 46dc45f 42d2ab3
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Thu Mar 16 00:31:32 2023 +0100

    feat: update setHeader value

[33mcommit 46dc45fd6fa8359b452e25269cb21390fff3c48a[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Thu Mar 16 00:30:42 2023 +0100

    feat: update setHeader value

[33mcommit 42d2ab3a1c2514afd32ad82313fbfe1550370200[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Thu Mar 16 00:18:23 2023 +0100

    feat: update setHeader value

[33mcommit ddc809a18a0d413dbd9f484387b1b617db94b280[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Thu Mar 16 00:06:26 2023 +0100

    feat: update setHeader value

[33mcommit 5988ac5b4c2fe14731daeb4c2e9d9820766e7ccd[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Thu Mar 16 00:02:31 2023 +0100

    feat: update setHeader value

[33mcommit c4f7fb620dfb71c09a44730296ae3c4737e8cf73[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Wed Mar 15 23:59:38 2023 +0100

    feat: update setHeader value

[33mcommit 8447dd5f04e0d94cd9bdfea20e4520868c872b24[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Wed Mar 15 23:55:59 2023 +0100

    feat: update setHeader value

[33mcommit 3a090113369bbe54e6e3c36c415a42a735006a7d[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Wed Mar 15 23:53:02 2023 +0100

    feat: update setHeader value

[33mcommit e1124f30c603a2b1e6fdf9606826246e7d6bc75b[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Wed Mar 15 23:47:39 2023 +0100

    feat: update setHeader value

[33mcommit f4973e1fdedd1c1202a6207205119da6e2b9bc07[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Wed Mar 15 23:45:10 2023 +0100

    feat: update api routes

[33mcommit f387471112d64c63bb1e908f3f4ebd62f07597d3[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Tue Mar 14 23:30:10 2023 +0100

    feat: change res.cookie to setHeader

[33mcommit 3ee852de36f0290aef427d74bd73ed6e7f4b5541[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Mon Mar 13 21:32:55 2023 +0100

    feat: set access control allow origin to true

[33mcommit 677cc69baa22fb41430a044ca6db53c52b5af7e0[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Fri Mar 10 16:09:05 2023 +0100

    feat: set sameSite to none and secure true in AuthToken

[33mcommit a0a62d719eebd725922d6b4d482af5f1ae733a97[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Thu Mar 9 00:47:29 2023 +0100

    feat: update vercel.json file

[33mcommit b2ad008bbc071801a8b85d2afac94671fb1683c9[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Wed Mar 8 16:46:00 2023 +0100

    feat: change vercel server root

[33mcommit 083a73e7ac8afe4e1ee9b05506b215f587274afb[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Wed Mar 8 16:07:15 2023 +0100

    feat: add vercel config

[33mcommit a24d6db85557c4e7016416c15cde81d0eab811f1[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Tue Mar 7 17:08:08 2023 +0100

    feat: change error codes in login api path

[33mcommit be0b06147fad28b632d9a8b5f81062583139c8d3[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Tue Feb 7 13:27:28 2023 +0100

    feat: update .env.examples and mongoDbUrl with database connection string

[33mcommit e9d7f3a291f0e7905efc417cafd0f8da1331083e[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sun Feb 5 21:30:56 2023 +0100

    feat: add TZ to docker.env

[33mcommit c522cee385d380d7b5c0cbb9e26228384566aa81[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sun Feb 5 21:26:22 2023 +0100

    feat: add dayjs package, add timezone to docker

[33mcommit 93b8c9c96a063b5879ac0aefe891fb0eeb59d2e1[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Thu Feb 2 22:17:21 2023 +0100

    feat: change task creation time to local

[33mcommit e89d1860731ed078473c9fd77e7ecac6b0550054[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Wed Feb 1 01:55:54 2023 +0100

    fix: fix task data validation

[33mcommit 1fb5447dac3cf559156d0b959858854b7721de2b[m
Merge: 5414dbd 8eb66c4
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sun Jan 29 23:47:52 2023 +0100

    Merge pull request #7 from Dawken/Docker
    
    feat: change task format data and logout cookie destroy

[33mcommit 8eb66c4d1cbccfe07405c4a49cc3b4c338589af2[m[33m ([m[1;31morigin/Docker[m[33m)[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sun Jan 29 23:46:21 2023 +0100

    feat: change task format data and logout cookie destroy

[33mcommit 5414dbde3c2871823160dbdeddbbe2fd8fa440df[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sun Jan 29 19:53:10 2023 +0100

    Update README.md

[33mcommit e98e3a2497684525eb2034dfa8dfd611662b2a8b[m
Merge: a9ad4fa f06aa6e
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sun Jan 29 19:41:46 2023 +0100

    Merge pull request #6 from Dawken/Docker
    
    feat: update documentation with docker setup

[33mcommit f06aa6e42df28558e160fee1a963d89e459beede[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sun Jan 29 19:39:29 2023 +0100

    feat: update documentation with docker setup

[33mcommit a9ad4fa2a4baea142525b9c8e6855260f970bea0[m
Merge: 888c7cd 8931aeb
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sun Jan 29 18:22:16 2023 +0100

    Merge pull request #5 from Dawken/Docker
    
    feat: create docker.env file, update documentation

[33mcommit 8931aebf9009815486d863fabd3cf92e83a39fe8[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sun Jan 29 18:21:37 2023 +0100

    feat: create docker.env file, update documentation

[33mcommit 888c7cd9b94bf1e839a9a4b157a1a63521624761[m
Merge: 0267c10 9116faf
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sat Jan 28 18:02:27 2023 +0100

    Merge pull request #4 from Dawken/Docker
    
    feat: add docker

[33mcommit 9116fafee426ba744ccb33a42d09e7f232f786d4[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sat Jan 28 16:46:05 2023 +0100

    feat: implement code style rules to whole project

[33mcommit ba92396dd71ce4eb0de7a25f4313563a650c79cf[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Fri Jan 27 18:02:06 2023 +0100

    fix: fix register gender and last name validation

[33mcommit 58220b563e7b55818789e3f5104e2fa2e152f515[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Fri Jan 27 17:40:55 2023 +0100

    feat: delete /data folder, change .env.examples mongodb_database value

[33mcommit 59d8961cb4cef7bf0d0197b2f301b4bb32497368[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Fri Jan 27 01:56:54 2023 +0100

    feat: fix docker image, update .env.examples, add lint staged

[33mcommit a13f83bddf652e6314bd8f0c32c6be6cf5bd83e5[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Wed Jan 25 16:47:12 2023 +0100

    feat: add network bridge, mongoDbUrl creator and errorCode to register

[33mcommit 181e932add2e82052d25e9eca2bd222fa29cc650[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Tue Jan 24 15:00:03 2023 +0100

    feat: add mongoDb url creator, update .env.examples and documentation

[33mcommit 55322b7e975f669c639fadf6a9cf3b07fcb8fa05[m
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Sun Jan 22 20:38:19 2023 +0100

    feat: add docker

[33mcommit 0267c10e3070b46da77ae7f13e9675fde0cf5af1[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Sun Jan 15 01:01:05 2023 +0100

    feat: change syntax, delete unused text validation, change name of taskDTO and login validation, fix eslint

[33mcommit f02738d1875f50fe62ae258fc486efdaf3f869b4[m
Author: Dawid Domiński <dawid.dominski1125@gmail.com>
Date:   Sat Jan 14 14:05:06 2023 +0100

    feat: add body validation to tasks and register/login requests

[33mcommit ee54a8bfb81b3984f3e7efbadbe83f03a72e716c[m
Merge: 3d14748 8c15449
Author: Dawken <dawid.dominski1125@gmail.com>
Date:   Wed Jan 11 2